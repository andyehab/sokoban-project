
#include "levelan.hpp"
#include "boxan.h"
#include "playaa.hpp"
#include "MenuVector.h"
#include <fstream>
#include <iostream>
#include<SFML/Audio.hpp>
#include<SFML/Graphics.hpp>
#include<cstdlib>
//#include "ResourcePath.hpp"

using namespace std;
using namespace sf;


void loadersilent(); //load Silent songs
void swapstate(Sprite& x, Texture on, Texture off); //swpas state from sounds on to off
enum game_song { game_s, jump_s, laugh_s }; //Different Sounds

//void get_positionmark;

//Global Constants
const int w1 = 35;

//functions
int checkbox(int, int, int, std::vector<box>& b);



void loader(game_song, int);
SoundBuffer *soundloader[4];

vector <string> A = { "level11map.txt", "level22map.txt", "level3map.txt", "level4map.txt", "level55map.txt" };

vector <int> counter = { 10, 100, 160,40,52 };

struct position
{
	int rows;
	int cols;

};

//Da hander
int main()
{
	int levelnum = 0;
	int moves = 0;
	int lost = 0;
	string currLevel = A[levelnum]; //needed throught out the file
	bool done = false;

	Font font;
	if (!font.loadFromFile("sansation.ttf")) {
		return EXIT_FAILURE;
	}

	Text cc("Counter:", font, 50);
	cc.setPosition(50, 800);
	cc.setColor(Color::Green);
	cc.setStyle(Text::Bold);

	Text numb;
	numb.setFont(font);
	numb.setCharacterSize(50);
	numb.setPosition(150, 900);
	numb.setColor(Color::Green);
	numb.setStyle(Text::Italic);

	Text lives("Lives:", font, 50);
	lives.setPosition(300, 800);
	lives.setColor(Color::Green);
	lives.setStyle(Text::Bold);

	Text noflives;
	noflives.setFont(font);
	noflives.setCharacterSize(50);
	noflives.setPosition(350, 900);
	noflives.setColor(Color::Green);
	noflives.setStyle(Text::Italic);

	Text loser("U LOSE", font, 50);
	loser.setPosition(150, 800);
	loser.setColor(Color::Green);
	loser.setStyle(Text::Bold);
	loser.setCharacterSize(40);
	loser.setStyle(Text::Bold);


	Text wintxt("U WIN", font, 50);
	wintxt.setPosition(150, 800);
	wintxt.setColor(Color::Green);
	wintxt.setStyle(Text::Bold);
	wintxt.setCharacterSize(40);
	wintxt.setStyle(Text::Bold);

	// The Story TimeLine!!!

	string sentence[5] = { "There once was a \n dragon that fell in love...","He decided to make a move and \n ask the love of his life out", "But he got rejected", "And the anger ate his soul away", "NOW HE FEEDS ON \n HUMANS FOR FUN" };

	vector <Text> text;
	text.resize(5);
	for (int i = 0; i<5; i++) {
		text[i].setFont(font);
		text[i].setPosition(105, 150);;
		text[i].setColor(Color::Red);
		text[i].setString(sentence[i]);
		text[i].setStyle(Text::Italic);
		text[i].setCharacterSize(40);
	}




	// Play the music
	SoundBuffer game, moving, laughing;
	Sound plays, jump, laugh;
	soundloader[0] = &game;
	plays.setBuffer(game);
	soundloader[1] = &moving;
	soundloader[2] = &laughing;
	jump.setBuffer(moving);
	laugh.setBuffer(laughing);
	//Texture background
	Texture back;

	//default backgrouund
	Sprite background(back);

	if (!back.loadFromFile("gray city.jpeg"))
	{
		return EXIT_FAILURE;

	}
	background.setTextureRect(IntRect(0, 0, 1000, 1000));
	background.setPosition(0, 0);
	background.setScale(Vector2f(2.5f, 2.5f));
	back.setSmooth(true);

	//Seconds needed background
	Texture back2;

	Sprite background2(back2);

	if (!back2.loadFromFile("blue_skies.jpg"))
	{
		return EXIT_FAILURE;

	}
	background2.setTextureRect(IntRect(0, 0, 1000, 1000));
	background2.setPosition(0, 0);
	background2.setScale(Vector2f(2.5f, 2.5f));
	back2.setSmooth(true);

	//Loading player texture
	Texture play;
	if (!play.loadFromFile("monster sprite.png"))
	{
		return EXIT_FAILURE;

	}
	//    Sprite player(play);
	//    player.setTextureRect(IntRect(0, 0, 35, 35));
	//player.setScale(Vector2f(2.5f, 2.f));

	//Texture for box


	//Texture for themes
	Texture t;
	if (!t.loadFromFile("Theme 2.png"))
		return EXIT_FAILURE;
	Texture t2;
	if (!t2.loadFromFile("Monster themes.png"))
		return EXIT_FAILURE;


	level l(currLevel, t);
	l.setdisp(30, 30);
	IntRect i(35, 0, 35, 35);
	IntRect j(105, 0, 35, 35);
	IntRect k(70, 0, 35, 35);
	IntRect m(0, 0, 35, 35);
	IntRect bx(140, 0, 35, 35);
	IntRect pl(0, 0, 35, 35);


	RenderWindow window(VideoMode(1000, 1000), "Sokoban Game");
	l.setemptychar(' ', k); //set transparent
	l.enterShape('#', i);
	l.enterShape('s', j);
	l.enterShape('m', m);
	l.setTileSize(80, 80);


	l.setlevel();


	// BOX take from file to take rows and cols of box and player
	ifstream bp_in;

	bp_in.open(currLevel);


	//Declaration of boxes

	int nofboxes = 0;

	//Texture texture;
	string str;


	for (int i = 0; i < l.getrow() + 2; i++)
	{
		getline(bp_in, str);

	}

	bp_in >> nofboxes;
	cout << nofboxes;

	//Number of Boxes
	std::vector<position> p(nofboxes + 1);

	std::vector<box> b(nofboxes + 1);
	for (int i = 0; i < nofboxes + 1; i++)
	{
		bp_in >> p[i].rows;
		bp_in >> p[i].cols;
		//The Positions of the boxes fetched from the file


	}

	for (int i = 0; i < nofboxes; i++)
	{
		//Setting up the logic behind the characters vector needed to retrict movement

		b[i].setrow(p[i].rows);
		b[i].setcol(p[i].cols);
		b[i].setWinChar('m');
		b[i].setStopChar('#');
		b[i].setOKChar('s');
		b[i].setdisplacement(l);
		b[i].setPos();
		b[i].setTextureofBox(t);
		b[i].createSprite(bx);
		b[i].setscale(80, 80);
		b[i].setTime(3, .2);
	}
	//Setting up the logic behind the characters vector needed to retrict movement
	playaa player(p[nofboxes].rows, p[nofboxes].cols);
	player.setTileSize(80, 80);
	player.setStopChar('#');
	player.setOKChar('s');
	player.setdisplacement(l);
	player.setPos();
	player.setTextureofPlayaa(play);
	player.createSprite(pl);
	player.setTileSize(80, 80);


	//Right
	player.setRightIntRect(IntRect(32, 64, 32, 32));
	player.setRightIntRect(IntRect(32 * 2, 64, 32, 32));
	player.setRightIntRect(IntRect(32 * 3, 64, 32, 32));

	//Left
	player.setLeftIntRect(IntRect(32, 32, 32, 32));
	player.setLeftIntRect(IntRect(32 * 2, 32, 32, 32));
	player.setLeftIntRect(IntRect(32 * 3, 32, 32, 32));

	//up
	player.setUpIntRect(IntRect(32, 96, 32, 32));
	player.setUpIntRect(IntRect(32 * 2, 96, 32, 32));
	player.setUpIntRect(IntRect(32 * 3, 96, 32, 32));

	//down
	player.setDownIntRect(IntRect(32, 0, 32, 32));
	player.setDownIntRect(IntRect(32 * 2, 0, 32, 32));
	player.setDownIntRect(IntRect(32 * 3, 0, 32, 32));

	bp_in.close();

	Event event;
	Clock clock;
	float time = .4;


	//Intro Move

	menu first(window.getSize().x, window.getSize().y);
	first.setCharSize(32);
	first.setoptions("Enter Game");
	first.setoptions("Options");
	first.setoptions("Exit");
	first.setFont("Anke.ttf");
	first.setback( "maze menu.jpg");

	//options menu
	menu optionsmenu(window.getSize().x, window.getSize().y);
	optionsmenu.setCharSize(32);
	optionsmenu.setoptions("Sounds ON/OFF");
	optionsmenu.setoptions("About US! :)");
	optionsmenu.setoptions("Previous Menu");
	optionsmenu.setFont("Anke.ttf");
	optionsmenu.setback("maze menu.jpg");

	//Choose story menu, we were planning to have 2 stories
	menu storymenu(window.getSize().x, window.getSize().y);
	storymenu.setCharSize(32);
	storymenu.setoptions("Story 1");
	storymenu.setoptions("Previous Menu");
	storymenu.setFont("Anke.ttf");
	storymenu.setback("maze menu.jpg");




	Image in;
	in.loadFromFile("blue.png");
	Texture intex;
	intex.loadFromImage(in);
	Sprite intextspr;
	intextspr.setTexture(intex);
	intextspr.setPosition(850, 150);
	intextspr.setScale(Vector2f(2.f, 2.f));


	Mouse mouse;

	Image inn;
	inn.loadFromFile("red.png");
	Texture inntex;
	inntex.loadFromImage(inn);
	Sprite inntextspr;
	inntextspr.setTexture(inntex);
	inntextspr.setPosition(850, 300);
	inntextspr.setScale(Vector2f(2.f, 2.f));

	//Sound On Texture 
	Image soundon;
	soundon.loadFromFile("speaker on.png");
	Texture soundontex;
	soundontex.loadFromImage(soundon);
	Sprite soundonspr;
	soundonspr.setTexture(soundontex);
	soundonspr.setPosition(850, 450);
	soundonspr.setScale(Vector2f(2.f, 2.f));
	//Sound OFF Texture
	Image soundoff;
	soundoff.loadFromFile("speaker off.png");
	Texture soundofftext;
	soundofftext.loadFromImage(soundoff);




	vector<String> imagesnames;
	imagesnames.resize(5);
	imagesnames = { "monster love.jpeg", "worried monster.jpg" ,"angry monster.png", "monster eat humans.jpg", "gray city.jpeg"};
	
	
	//Vector of images to built textures and sprites later on
	vector<Image> imagesbgd;
	imagesbgd.resize(5);
	for (int i = 0; i < imagesnames.size(); i++)
	{
		imagesbgd[i].loadFromFile(imagesnames[i]);
	}

	vector<Texture> imagesText;
	imagesText.resize(5);

	for (int i = 0; i < imagesnames.size(); i++)
	{
		imagesText[i].loadFromImage(imagesbgd[i]);
	}
	vector<Sprite> backgrounds;
	backgrounds.resize(5);
	for (int i = 0; i < imagesnames.size(); i++)
	{
		backgrounds[i].setTexture(imagesText[i]);
		backgrounds[i].setScale(2.f, 2.f);
	}
	

	//Boolean to control the menu

	bool showFirst = true;
	bool showOptions = false;
	bool GoMenuBack = false;
	bool showStories = false;
	bool showgame = false;

	Clock menuclock;
	Time menutime;

	loader(game_s, levelnum);
	plays.play();
	plays.setLoop(true);

	Clock winclock;

	bool flag1 = false, flag = false;

	vector<int> boxmovements;
	int nomove = -1;





	while (window.isOpen())
	{
		float movementSpeed = 3.5;
		int rp, cp;

		rp = player.getrow();
		cp = player.getcol();


		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed || event.key.code == Keyboard::Escape)
				window.close();
			switch (event.KeyPressed)
			{
			case Event::Closed:
				window.close();
				break;
			case Event::KeyPressed:

				if (!flag)
					if (clock.getElapsedTime().asSeconds()>time)
					{

						if ((sf::Mouse::isButtonPressed(Mouse::Left)) && (intextspr.getGlobalBounds().contains(mouse.getPosition(window).x, mouse.getPosition(window).y)))
						{
							if (moves>0)
								moves--;

							clock.restart();

							if (boxmovements.size()>0 && boxmovements.back() != nomove)
								b[boxmovements.back()].undoMove();

							player.undoMovep();

							if (boxmovements.size()>0)
								boxmovements.pop_back();
						}

						if ((Keyboard::isKeyPressed(Keyboard::Up)) && (showgame))
						{
							clock.restart();
							int bindex = checkbox(rp - 1, cp, nofboxes, b);
							if (bindex != -1) {
								int temp = checkbox(rp - 2, cp, nofboxes, b);
								if (temp == -1) {
									if (moves<counter[levelnum])
										moves++;

									if (b[bindex].movebox(-1, 0, l))
									{
										if (b[bindex].checkwin(l))
										{

											player.move(-1, 0, l);
											loader(laugh_s, levelnum);
											laugh.play();
										}
										else {
											player.move(-1, 0, l);
											loader(jump_s, levelnum);
											jump.play();
										}


									}

								}
							}
							else {
								player.move(-1, 0, l);
								if (moves<counter[levelnum])
									moves++;
							}

							boxmovements.push_back(bindex);

						}
						// down movement
						if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) && (showgame))
						{


							clock.restart();
							int bindex = checkbox(rp + 1, cp, nofboxes, b);
							if (bindex != -1) {
								int temp = checkbox(rp + 2, cp, nofboxes, b);
								if (temp == -1) {
									if (moves<counter[levelnum])
										moves++;
									if (b[bindex].movebox(1, 0, l))
									{
										if (b[bindex].checkwin(l))
										{

											player.move(1, 0, l);
											loader(laugh_s, levelnum);
											laugh.play();
										}
										else {
											player.move(1, 0, l);
											loader(jump_s, levelnum);
											jump.play();
										}
									}

								}
							}
							else {
								player.move(1, 0, l);
								if (moves<counter[levelnum])
									moves++;
							}

							boxmovements.push_back(bindex);

						}
						// left movement
						if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) && (showgame))
						{

							clock.restart();
							int bindex = checkbox(rp, cp - 1, nofboxes, b);
							if (bindex != -1) {
								int temp = checkbox(rp, cp - 2, nofboxes, b);
								if (temp == -1) {
									if (moves<counter[levelnum])
										moves++;
									if (b[bindex].movebox(0, -1, l))
									{
										if (b[bindex].checkwin(l))
										{

											player.move(0, -1, l);
											loader(laugh_s, levelnum);
											laugh.play();
										}
										else {
											player.move(0, -1, l);
											loader(jump_s, levelnum);
											jump.play();
										}

									}
								}
							}
							else {
								player.move(0, -1, l);
								if (moves<counter[levelnum])
									moves++;
							}

							boxmovements.push_back(bindex);

						}
						// right movement
						if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) && (showgame))
						{

							clock.restart();
							int bindex = checkbox(rp, cp + 1, nofboxes, b);
							if (bindex != -1) {
								int temp = checkbox(rp, cp + 2, nofboxes, b);
								if (temp == -1) {
									if (moves<counter[levelnum])
										moves++;

									if (b[bindex].movebox(0, 1, l))
									{
										if (b[bindex].checkwin(l))
										{

											player.move(0, 1, l);
											loader(laugh_s, levelnum);
											laugh.play();
										}
										else {
											player.move(0, 1, l);
											loader(jump_s, levelnum);
											jump.play();
										}


									}
								}
							}

							else {
								player.move(0, 1, l);
								if (moves<counter[levelnum])
									moves++;
							}

							boxmovements.push_back(bindex);
						}


						if ((sf::Mouse::isButtonPressed(Mouse::Left)) && (soundonspr.getGlobalBounds().contains(mouse.getPosition(window).x, mouse.getPosition(window).y)))
						{
							swapstate(soundonspr, soundontex, soundofftext);
						}

						if ((sf::Mouse::isButtonPressed(Mouse::Left)) && (inntextspr.getGlobalBounds().contains(mouse.getPosition(window).x, mouse.getPosition(window).y)))
						{
							moves = 0;
							clock.restart();
							lost++;
							l.setNewFile(A[levelnum], window);

							bp_in.open(A[levelnum]);

							nofboxes = 0;

							for (int i = 0; i < l.getrow() + 2; i++)
							{
								getline(bp_in, str);
							}

							bp_in >> nofboxes;
							cout << nofboxes;

							p.clear();
							p.resize(nofboxes + 1);

							b.clear();
							b.resize(nofboxes + 1);


							for (int i = 0; i < nofboxes + 1; i++)
							{
								bp_in >> p[i].rows;
								bp_in >> p[i].cols;
								
							}

							for (int i = 0; i < nofboxes; i++)
							{

								b[i].setTileSize(80, 80);
								b[i].setrow(p[i].rows);
								b[i].setcol(p[i].cols);
								b[i].setWinChar('m');
								b[i].setStopChar('#');
								b[i].setOKChar('s');
								b[i].setdisplacement(l);
								if (levelnum<1)
									b[i].setTextureofBox(t);
								else if (levelnum<4)
									b[i].setTextureofBox(t2);
								b[i].createSprite(bx);
								b[i].setscale(80, 80);
								b[i].setPos();
								b[i].setTime(3, .2);

							}

							player.setnewPos(p[nofboxes].rows, p[nofboxes].cols);
							bp_in.close();




						}





			case Event::KeyReleased:
				menutime = menuclock.getElapsedTime();
				menutime.asSeconds();
				if (menutime > seconds(0.3))

				{
					menuclock.restart();

					switch (event.key.code)
					{
					case Keyboard::Up:
						if (showFirst)
							first.moveup();
						else
							if (showOptions)
								optionsmenu.moveup();
							else  if (showStories)
								storymenu.moveup();


						clock.restart();
						break;

					case Keyboard::Down:

						if (showFirst) first.movedown();
						else if (showOptions)
							optionsmenu.movedown();
						else
							if (showStories)
								storymenu.movedown();
						clock.restart();
						break;

					case Keyboard::Return:
					{
						switch (first.getselecteditem())
						{
						case 0:
							if (showFirst == true)
							{
			
								showFirst = false;
								showgame = false;
								showStories = true;
								showOptions = false;
							}
							else
								switch (storymenu.getselecteditem())
								{
								case 0:
									
									showFirst = false;
									showgame = true;
									showStories = false;
									showOptions = false;
									break;
	
								case 1:
									showFirst = true;
									showStories = false;
									break;
								}
							break;
						case 1:
							cout << "Options";
							showFirst = false;
							showOptions = true;
							showStories = false;
							switch (optionsmenu.getselecteditem())
							{
							case 0:
								loadersilent();
								break;
							case 1:
								cout << "There was an intention to build this, but no time!";
								break;
							case 2:
								showFirst = true;
								showOptions = false;
								break;

							}


							break;
						case 2:
							window.close();

							break;
						}
					}
					}
				}
					}


			}

		}

		numb.setString(to_string(counter[levelnum] - moves));
		noflives.setString(to_string(3 - lost));

		float tt = .1;
		player.update(tt);
		for (int i = 0; i < nofboxes; i++)
			b[i].update();


		if (!flag)
		{
			flag = true;
			for (int i = 0; i < nofboxes; i++)
				if (!b[i].checkwin(l))
					flag = false;

			winclock.restart();
		}


		if (flag && !done) {
			if (winclock.getElapsedTime().asSeconds()>float(5))
			{
				moves = 0;

				clock.restart();
				flag = false;


				if (levelnum < 4) {
					if (levelnum<1) {
						levelnum++;
						l.setNewFile(A[levelnum], window);
						bp_in.open(A[levelnum]);


						nofboxes = 0;

						for (int i = 0; i < l.getrow() + 2; i++)
						{
							getline(bp_in, str);
						}

						bp_in >> nofboxes;
						cout << nofboxes;

						p.clear();
						p.resize(nofboxes + 1);

						b.clear();
						b.resize(nofboxes + 1);


						for (int i = 0; i < nofboxes + 1; i++)
						{
							bp_in >> p[i].rows;
							bp_in >> p[i].cols;
						}

						for (int i = 0; i < nofboxes; i++)
						{
							b[i].setrow(p[i].rows);
							b[i].setcol(p[i].cols);
							b[i].setWinChar('m');
							b[i].setStopChar('#');
							b[i].setOKChar('s');
							b[i].setdisplacement(l);
							b[i].setPos();
							b[i].setTextureofBox(t);
							b[i].createSprite(bx);
							b[i].setscale(80, 80);

							b[i].setTime(3, .2);
						}
					}
					else {
						levelnum++;
						l.setNewFile(A[levelnum], window);
						bp_in.open(A[levelnum]);
						l.setTexture(t2);

						nofboxes = 0;

						for (int i = 0; i < l.getrow() + 2; i++)
						{
							getline(bp_in, str);
						}

						bp_in >> nofboxes;
						cout << nofboxes;

						p.clear();
						p.resize(nofboxes + 1);

						b.clear();
						b.resize(nofboxes + 1);


						for (int i = 0; i < nofboxes + 1; i++)
						{
							bp_in >> p[i].rows;
							bp_in >> p[i].cols;
							
						}

						for (int i = 0; i < nofboxes; i++)
						{
							b[i].setrow(p[i].rows);
							b[i].setcol(p[i].cols);
							b[i].setWinChar('m');
							b[i].setStopChar('#');
							b[i].setOKChar('s');
							b[i].setdisplacement(l);
							b[i].setPos();
							b[i].setTextureofBox(t2);
							b[i].createSprite(bx);
							b[i].setscale(80, 80);

							b[i].setTime(3, .2);
						}
					}


					player.setnewPos(p[nofboxes].rows, p[nofboxes].cols);
					bp_in.close();
				}
			}
			else if (flag&&levelnum == 4) {

				done = true;
				l.clear();

			}
		}

		if (counter[levelnum] == moves && !flag) {
			lost++;
			moves = 0;


			l.setNewFile(A[levelnum], window);
			bp_in.open(A[levelnum]);

			nofboxes = 0;

			for (int i = 0; i < l.getrow() + 2; i++)
			{
				getline(bp_in, str);
			}

			bp_in >> nofboxes;

			p.clear();
			p.resize(nofboxes + 1);

			b.clear();
			b.resize(nofboxes + 1);


			for (int i = 0; i < nofboxes + 1; i++)
			{
				bp_in >> p[i].rows;
				bp_in >> p[i].cols;
				cout << p[i].rows;
				cout << p[i].cols;
			}

			for (int i = 0; i < nofboxes; i++)
			{
				b[i].setTileSize(80, 80);
				b[i].setrow(p[i].rows);
				b[i].setcol(p[i].cols);
				b[i].setWinChar('m');
				b[i].setStopChar('#');
				b[i].setOKChar('s');
				b[i].setdisplacement(l);
				if (levelnum<1)
					b[i].setTextureofBox(t);
				else if (levelnum<4)
					b[i].setTextureofBox(t2);
				b[i].createSprite(bx);
				b[i].setscale(80, 80);
				b[i].setPos();
				b[i].setTime(3, .2);
			}




			player.setnewPos(p[nofboxes].rows, p[nofboxes].cols);
			bp_in.close();



		}




		if ((!showFirst) && (showgame) && (!showOptions) && (!showStories) && (!done))
		{
			if(levelnum <2)
				window.draw(background2);

		else
			window.draw(background);
			l.drawlevel(window);

			for (int i = 0; i < nofboxes; i++) {
				b[i].DrawThatBox(window);
			}
			player.DrawThatPlayaa(window);
			window.draw(cc);
			window.draw(numb);
			window.draw(lives);
			window.draw(noflives);
			window.draw(intextspr);
			window.draw(inntextspr);
			window.draw(soundonspr);

		}
		//implemting menu
		if (showFirst)
			first.menudraw(window);
		else
			if (showOptions)
				optionsmenu.menudraw(window);
			else
				if (showStories)
					storymenu.menudraw(window);

		if (!done&&flag&&showgame) //Background of the level
		{
			window.draw(backgrounds[levelnum]);
			window.draw(text[levelnum]);
		}
		else
		if (done) {   //Wins the level
			window.draw(background);
			window.draw(wintxt);
		}
		else

		if (!done && !flag && (3 - lost == 0)) { //Loses the level
			window.draw(background);
			window.draw(loser);
		}



		window.display();

		window.clear(Color::Black);




	}
}

int checkbox(int row, int col, int num, std::vector<box>& b) //Checking for a box at a specific location
{
	int i = 0;
	while (i < num)
	{
		if ((b[i].getrow() == row) && (b[i].getcol() == col))
			return i;
		else
			i++;
	}

	return -1;
}


void loader(game_song loads, int num)  //loadind of music
{
	switch (loads)
	{
	case game_s:
		if (num >= 2)
			soundloader[0]->loadFromFile("Zombie-Game.ogg");
		else
			soundloader[0]->loadFromFile("Adam_Selzer_-_Happy_Times.ogg");

		break;

	case jump_s:
		if (num >= 2)
			soundloader[1]->loadFromFile("Jump-SoundBible.com-1007297584.wav");
		else
			soundloader[1]->loadFromFile("Cartoon Hop-SoundBible.com-553158131.wav");

		break;
	case laugh_s:
		if (num >= 2)
			soundloader[2]->loadFromFile("Evil_laugh.ogg");
		else
			soundloader[2]->loadFromFile("Oh Yeah Female Voice-SoundBible.com-187589915.wav");
		break;


	}

}



void loadersilent() //Turns off Music by playing a silent track
{
	for (int i = 0; i < 3; i++)
		soundloader[0]->loadFromFile("5min.ogg");
}

void swapstate(Sprite& x, Texture on, Texture off) 
{

	x.setTexture(off);
	loadersilent();

}