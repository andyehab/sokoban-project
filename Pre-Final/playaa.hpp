//
//  playaa.hpp
//  PROJECT1
//
//  Created by Sarah Darwish on 5/8/16.
//  Copyright © 2016 Sarah Darwish. All rights reserved.
//

#ifndef playaa_hpp
#define playaa_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <vector>
#include "levelan.hpp"
using namespace sf;

class playaa{
private:
    
    enum direction{Right, Left, Up, Down};
    Texture playaaTexture;
    int TileSize;
    char OKChar;
    char StopChar;
    char WinChar;
    int row, col;
    Sprite player;
    std::vector<IntRect> right;
    std::vector<IntRect> left;
    std::vector<IntRect> up;
    std::vector<IntRect> down;
    
    bool movement=false;
    Clock clock;
    direction dir;
    int speed, savevectorindex;
   
    int dispX;
    int dispY;
	int tileSizew = 0, tileSizeh = 0;
	float scalefactorx = 1;
	float scalefactory = 1;
    vector<pair<int, int>> undop;
    void moveP(int, int);

public:
    
    playaa(int, int);
    void update(float&);
    bool move(int,int, level&);
    void setTextureofPlayaa(Texture);
    void setPos();
    void setTileSize(int, int);
    void setOKChar(char);
    void setStopChar(char);
    void createSprite(IntRect ir);
    void DrawThatPlayaa(RenderWindow & w);
    void setrow(int);
    void setcol(int);
    int getrow();
    int getcol();
    void setRightIntRect(IntRect);
    void setLeftIntRect(IntRect);
    void setUpIntRect(IntRect);
    void setDownIntRect(IntRect);
    void setdisplacement(level &l);
    void setnewPos(int,int);
    char getOKchar();
    char getWinchar();
    void setWinChar(char);
	void setscale(int, int);
    void undoMovep();
};

#endif /* playaa_hpp */
