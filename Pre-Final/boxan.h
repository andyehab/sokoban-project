#include <SFML/Graphics.hpp>
using namespace sf;
#include "levelan.hpp"

#ifndef box_h
#define box_h

class box
{
private:

	Texture boxTexture;
	int TileSize,TileSizex=1, TileSizey=1;
	float scaleX=1, scaleY=1;
	bool IsBoxMoving;
	char WinChar;
	char OKChar;
	char StopChar;
	bool win;
	int row, col;
	Sprite OneBox;
    int dispX;
    int dispY;
    vector<pair<int, int>> undo;
    Clock clock;
    int numOfMovements=1;
    float time=.05;
    bool move=false;
    int count=0;
    int changei, changej;
public:
	box();
	//box(int, int, Texture);

	void setTextureofBox(sf::Texture&);
	void setPos(); //Starting position in the screen, i, j.
	void setWinChar(char);
	void setOKChar(char);
	void setStopChar(char);
	void createSprite(IntRect ir);
	void DrawThatBox(RenderWindow & w); //Not sure yet about the parameters
    bool movebox(int, int,level&);
    void setrow(int);
    void setcol(int);
    int getrow();
    int getcol();
    bool checkwin(level&l);
    void setdisplacement(level &l);
	void setTileSize(int, int);
	void setscale(int, int);
    void undoMove();
    void setTime(int, float);
    void update();

};












#endif 

