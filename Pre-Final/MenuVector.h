#ifndef menuvector
#define menuvector
#include <vector>
#include <SFML/Graphics.hpp>
#include<string>
using namespace std;
using namespace sf;


class menu
{

private:
	vector<Text> option;
	Font font;
	int defaultsize = 32;
	int selectedindex=0;
	void SetText();
	int l; int h;
	Image back;
	Texture backtexture;
	Sprite backSprite;
	bool settedbefore = false;
	bool customSetUp = false;

public:
	menu(int, int);
	void setoptions(string x);
	void setFont(string x);
	void setCharSize(int x);
	void menudraw(RenderWindow& w);
	void setback(string);
	void SetTextCustomX(int);



	
	void moveup();
	void movedown();
	int getselecteditem();



};
#endif // !menuvector#pragma once
