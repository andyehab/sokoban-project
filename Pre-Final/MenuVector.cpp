#include "MenuVector.h"




menu::menu(int x, int y)
{
	l = x;
	h = y;
}

void menu::setoptions(string x)
{
	Text temp;
	temp.setString(x);
	option.push_back(temp);
}


void menu::setFont(string x)
{
	font.loadFromFile(x);

}

void menu::setCharSize(int x)
{
	defaultsize = x;
}


void menu::SetText()
{
	for (int i = 0; i < option.size(); i++)
	{
		option[i].setCharacterSize(defaultsize);
		option[i].setFont(font);
		option[i].setPosition(Vector2f((l / 2.0), h / (option.size() + 1) * (i+1)));
	}

	option[selectedindex].setColor(Color::Red);
	option[selectedindex].setStyle(Text::Bold);
	option[selectedindex].setCharacterSize(defaultsize + 15);
}

void menu::movedown()
{
	if (selectedindex + 1 <= option.size() - 1)
	{
		option[selectedindex].setColor(Color::White);
		option[selectedindex].setCharacterSize(defaultsize);
		option[selectedindex].setStyle(Text::Regular);
		selectedindex++;
		option[selectedindex].setColor(Color::Red);
		option[selectedindex].setStyle(Text::Bold);
		option[selectedindex].setCharacterSize(defaultsize + 15);
	}
}


void menu::moveup()
{
	if (selectedindex - 1 >= 0)
	{
		option[selectedindex].setColor(Color::White);
		option[selectedindex].setCharacterSize(defaultsize);
		option[selectedindex].setStyle(Text::Regular);
		selectedindex--;
		option[selectedindex].setColor(Color::Red);
		option[selectedindex].setStyle(Text::Bold);
		option[selectedindex].setCharacterSize(defaultsize + 15);
	}
}


void menu::menudraw(RenderWindow& w)
{
	if ((!settedbefore) && (!customSetUp))
	{
		SetText();
		settedbefore = true;
	}

	w.draw(backSprite);
	for (int i = 0; i < option.size(); i++)
		w.draw(option[i]);
}
int menu::getselecteditem()
{
	return selectedindex;
}

void menu::setback(string x)
{
	back.loadFromFile(x);
	backtexture.loadFromImage(back);
	backSprite.setTexture(backtexture);
	backSprite.setScale(2, 2);
}


void menu::SetTextCustomX(int x)
{

	
		for (int i = 0; i < option.size(); i++)
		{
			option[i].setCharacterSize(defaultsize);
			option[i].setFont(font);
			option[i].setPosition(x, h / (option.size() + 1) * (i + 1));
		}

		option[selectedindex].setColor(Color::Red);
		option[selectedindex].setStyle(Text::Bold);
		option[selectedindex].setCharacterSize(defaultsize + 15);

		customSetUp = true;
	
}