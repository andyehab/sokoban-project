//
//  level.hpp
//  PROJECT1
//
//  Created by Sarah Darwish on 5/6/16.
//  Copyright © 2016 Sarah Darwish. All rights reserved.
//

#ifndef level_hpp
#define level_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
using namespace sf;
using namespace std;

class level{
private:
    Sprite **p;
    char **ch;
    int rows;
    int cols;
    Texture texture;
    string file;
    vector<char> characters;
    vector <IntRect> intrect;
    char empty;
    ifstream input;
    int dispX;
    int dispY;
	int tileSizew = 0, tileSizeh = 0;
	float scalefactorx = 1, scalefactory = 1;
	
    
public:
    level(string x, Texture t);
    void enterShape(char c, IntRect& ir);
    void setcharbase();
    void setspritebase();
    void setemptychar(char c, IntRect & ir);
    int getrow();
    int getcol();
    void drawlevel(RenderWindow& w);
    char getChar(int a, int b);
    void setlevel();
    void clear();
    void setNewFile(string s, RenderWindow &w);
    void setdisp(int,int);
    int getDispX();
	int getDispY();
	void setTileSize(int, int);
	void setTexture(Texture t);


};



#endif /* level_hpp */
