//
//  playaa.cpp
//  PROJECT1
//
//  Created by Sarah Darwish on 5/8/16.
//  Copyright © 2016 Sarah Darwish. All rights reserved.
//

#include "playaa.hpp"
#include "levelan.hpp"

playaa::playaa(int a, int b)
{
    row=a;
    col=b;
    
};



void playaa::setTextureofPlayaa(Texture texture)
{
    playaaTexture = texture;
    
}

void playaa::setTileSize(int x, int y)
{

	if (tileSizew>0 && tileSizeh>0)
	{
		if (tileSizeh != y)
		{
			scalefactory = float(y) / tileSizeh;
			tileSizeh = y;
		}
		if (tileSizew != x)
		{
			scalefactorx = float(x) / tileSizew;
			tileSizew = x;
		}
		player.setScale(scalefactorx, scalefactory);
	}
	else {

		tileSizeh = y;

		tileSizew = x;
		setPos();

	}
}
void playaa::setrow(int v)
{
    row=v;
    
}
void playaa::setcol(int v)
{
    col=v;
    
}

void playaa::setdisplacement(level &l){
    dispX=l.getDispX();
    dispY=l.getDispY();
}


void playaa::setPos(){
    
    player.setPosition(col*tileSizew+dispX,row*tileSizeh+dispY);
    
}


//void box::setTopLeft(int, int, int, int);


void playaa::setRightIntRect(IntRect rightintrect)
{
    right.push_back(rightintrect);
}
void playaa::setLeftIntRect(IntRect leftintrect)
{
    left.push_back(leftintrect);
}
void playaa::setUpIntRect(IntRect upintrect)
{
    up.push_back(upintrect);
}
void playaa::setDownIntRect(IntRect downintrect)
{
    down.push_back(downintrect);
}



void playaa::setOKChar(char okchar)
{
    OKChar = okchar;
    
}


void playaa:: setStopChar(char Stop)
{
    StopChar = Stop;
}

void playaa::setWinChar(char MarkSymbol)
{
    WinChar = MarkSymbol;
}

void playaa::DrawThatPlayaa(RenderWindow & w)
{
    w.draw(player);
    
    //window.draw(OneBox); //Game should be rendered as "Window" here for this to work;
    
}

void playaa::createSprite(IntRect ir)
{
    player.setTexture(playaaTexture);
    player.setTextureRect(ir);
	tileSizew = ir.width;
	tileSizeh = ir.height;
    
}

int playaa::getrow(){
    return row;
}

int playaa::getcol(){
    return col;
}

bool playaa::move(int changei, int changej, level& l)
{
   if(l.getChar(row+changei,col+changej)!=StopChar)
   {
       pair<int, int> temp;
       
       temp.first=row;
       temp.second=col;
       
       undop.push_back(temp);
    
       if(changei>0)
           dir=Down;
       if(changei<0)
           dir=Up;
        if(changej>0)
            dir=Right;
       if(changej<0)
           dir=Left;
       switch (dir)
       {
           case Right:
               speed=tileSizew/right.size();
               break;
           case Down:
               speed=tileSizeh/down.size();\
               break;
           case Left:
               speed=tileSizew/left.size();
               break;
           case Up:
               speed=tileSizeh/up.size();
               break;
           default:
               break;
       }
       
       movement=true;
       savevectorindex=0;
       
       return true;
   }
    return false;
}

void playaa::moveP(int changei, int changej)
{
    

    if(changei>0)
        dir=Down;
    if(changei<0)
        dir=Up;
    if(changej>0)
        dir=Right;
    if(changej<0)
        dir=Left;
    switch (dir)
    {
        case Right:
            speed=tileSizew/right.size();
            break;
        case Down:
            speed=tileSizeh/down.size();
            break;
        case Left:
            speed=tileSizew/left.size();
            break;
        case Up:
            speed=tileSizeh/up.size();
            break;
        default:
            break;
    }
    
    movement=true;
    savevectorindex=0;
    

}

void playaa::update(float& time)
{
    if (movement) {
        Time tt;
        tt = seconds(time);
        
        if(clock.getElapsedTime()>tt)
        {
            clock.restart();
                switch(dir)
            {
                case Right:
                {
            player.move(speed, 0);
            player.setTextureRect(right[savevectorindex++]);
            if(savevectorindex==right.size())
            {
                movement=false;
                col++;
                setPos();
            }
                }
            break;
                case Left:
                {
                    player.move(-speed, 0);
                    player.setTextureRect(left[savevectorindex++]);
                    if(savevectorindex==left.size())
                    {
                        movement=false;
                        col--;
                        setPos();
                    }
                }
                    break;
                case Up:
                {
                    player.move(0,-speed);
                    player.setTextureRect(up[savevectorindex++]);
                    if(savevectorindex==up.size())
                    {
                        movement=false;
                        row--;
                        setPos();
                    }
                }
                    break;

                case Down:
                {
                    player.move(0,speed);
                    player.setTextureRect(down[savevectorindex++]);
                    if(savevectorindex==down.size())
                    {
                        movement=false;
                        row++;
                        setPos();
                    }
                }
                    break;

    }
        }
        
    }
}
void playaa::setnewPos(int a, int b) 
{
	player.setPosition(b*tileSizew + dispX, a*tileSizeh + dispY);
	row = a;
	col = b;

    if(undop.size()>0)
        undop.clear();
}
void playaa::setscale(int x, int y)
{
	player.setScale(x, y);
}



char playaa::getOKchar(){
    return OKChar;
}

char playaa::getWinchar(){
    return WinChar;
}

void playaa::undoMovep()
{
    
    if(undop.size()>0)
    {
        pair<int, int> temp;
        
        temp=undop.back();
        
        moveP(temp.first-row, temp.second-col);
        
        undop.pop_back();
    }
    
}



