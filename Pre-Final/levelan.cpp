//
//  level.cpp
//  PROJECT1
//
//  Created by Sarah Darwish on 5/6/16.
//  Copyright © 2016 Sarah Darwish. All rights reserved.
//

#include "levelan.hpp"
#include<cctype>

level::level(string s, Texture t) {

	file = s;
	input.open(file);
	string temp;
	if (input.fail())
		EXIT_FAILURE;
	else {
		texture = t;

		input >> rows;
		input >> cols;
        cout<<rows;
        cout<<cols;


		dispX = 0;
		dispY = 0;

	}


}

void level::enterShape(char c, IntRect & ir) {
	characters.push_back(c);
	intrect.push_back(ir);
	if (tileSizew == 0 && tileSizeh ==0)
		tileSizew=ir.width;
	tileSizeh = ir.height;
}

void level::setemptychar(char c, IntRect& ir) {
	empty = c;
	characters.push_back(c);
	intrect.push_back(ir);

}


void level::setcharbase() {
	//int count=0;
	ch = new char*[rows + 1];

	for (int i = 0; i < rows; i++)
		*(ch + i) = new char[cols + 1];


	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++) {
			ch[i][j] = empty;
		}

	string k;
	getline(input, k);
	if (k != '\n')
		getline(input, k);


	for (int i = 0; i < rows; i++)
	{
		int cnt = k.size();
		if (k[cnt - 1] == '\n')
			cnt--;
		for (int j = 0; j < cnt; j++) {
			ch[i][j] = k[j];
			cout << ch[i][j];
		}
		cout << endl;


		getline(input, k);

	}

	input.close();

}




void level::setspritebase() {

	p = new Sprite *[rows + 1];

	for (int i = 0; i < rows; i++) {
		*(p + i) = new Sprite[cols + 1];
	}

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++) {
			p[i][j].setTexture(texture);
			p[i][j].setScale(Vector2f(2.5, 2.5));
			p[i][j].setPosition(tileSizew*j + dispX, tileSizeh*i +dispY);
			

		}
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++) {
			char temp = ch[i][j];
			int k = 0;
			while (k < characters.size() && temp != characters[k]) {
				k++;
			}
			if (k < characters.size())
				p[i][j].setTextureRect(intrect[k]);
		}


}

int level::getrow() {
	return rows;
}

int level::getcol() {
	return cols;
}

void level::drawlevel(RenderWindow& w) 
{
	for (int i = 0;i < rows;i++)
		for (int j = 0;j < cols;j++)
			w.draw(p[i][j]);
}

char level::getChar(int a, int b) {
	char u = ch[a][b];
	return u;

}

void level::setlevel() {
	setcharbase();
	setspritebase();
}



void level::clear() {
	for (int i = 0; i < rows; i++) {
		delete[] * (p + i);
		delete[] * (ch + i);
	}
	delete[] p;
	delete[] ch;

	characters.clear();
	intrect.clear();


}

void level::setNewFile(string s, RenderWindow &w) {
	/*cout << '\n';
	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 9; ++j) {
			cout << *(*(ch + i) + j);
		}
		cout << '\n';
	}
*/
	if (p != NULL)
	{
		for (int i = 0; i < rows; i++) {
			delete[] * (p + i);
			delete[] * (ch + i);
		}
		delete[] p;
		delete[] ch;
	}

	w.clear();
	input.open(s);
	string temp; 
	if (input.fail())
		EXIT_FAILURE;
	else {
		input >> rows;
		input >> cols;
		setlevel();
	}
}
int level::getDispX()
{
	return dispX;
}
int level::getDispY()
{
	return dispY;
}
void level::setdisp(int a, int b) {
	dispX = a;
	dispY = b;
}


void level::setTileSize(int x, int y)
{

	if (tileSizew>0 && tileSizeh>0)
	{
		if (tileSizeh != y)
		{
			scalefactory = float(y) / tileSizeh;
			tileSizeh = y;
		}
		if (tileSizew != x)
		{
			scalefactorx = float(x) / tileSizew;
			tileSizew = x;
		}
	}
	else {

		tileSizeh = y;

		tileSizew = x;

	}
}

void level::setTexture(Texture t)
{
	texture = t;
}
