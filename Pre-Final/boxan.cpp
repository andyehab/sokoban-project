#include "boxan.h"
#include "levelan.hpp"
#include<iostream>
#include<fstream>
#include<SFML/Graphics.hpp>
using namespace std;
using namespace sf;


box::box()
{
    row=0;
    col=0;
	TileSizex = 0;
	TileSizey = 0;
    
};

//box::box(int r, int c, Texture boxT)
//{
//    
//
//}

void box::setTextureofBox(Texture& texture)
{
	boxTexture = texture;
}

void box::setTileSize(int v, int)
{
    if(TileSize>0)
    OneBox.setScale(v/TileSize, v/TileSize);
    TileSize=v;
}
void box::setrow(int v)
{
    row=v;
    
    if(undo.size()>0)
        undo.clear();
}
void box::setcol(int v)
{
    col=v;
    
    if(undo.size()>0)
        undo.clear();
}

void box::setdisplacement(level &l){
    dispX=l.getDispX();
    dispY=l.getDispY();
}


void box::setPos() //Starting position in the screen, i, j.
{
    OneBox.setPosition(col*TileSizex+dispX,row*TileSizey+dispY);
}


//void box::setTopLeft(int, int, int, int);


void box::setWinChar(char MarkSymbol)
{
	WinChar = MarkSymbol;
}


void box::setOKChar(char okchar)
{
    OKChar = okchar;

}


void box::setStopChar(char Stop)
{
	StopChar = Stop;
}

bool box::movebox(int changei, int changej, level& lev)
{
    if(lev.getChar(row+changei, col+changej)!=StopChar)
    {
        pair<int, int> temp;
        
        temp.first=row;
        temp.second=col;
        
        undo.push_back(temp);
        
        row+=changei;
        col+=changej;
        move=true;
        this->changei=changei;
        this->changej=changej;
        
        return true;
    }
    else return false;
}


void box::DrawThatBox(RenderWindow & w)
{
    w.draw(OneBox);
	
	//window.draw(OneBox); //Game should be rendered as "Window" here for this to work;

}



void box::createSprite(IntRect ir)
{
	OneBox.setTexture(boxTexture);
    OneBox.setTextureRect(ir);
	TileSizex = ir.width;
	TileSizey = ir.height;
}

int box::getrow(){
    return row;
}

int box::getcol(){
    return col;
}

bool box::checkwin(level& l){
    if(l.getChar(row,col)==WinChar)
        return true;
    else
        return false;
}

void box::setscale(int x, int y)
{
	float scalex = float(x) / TileSizex, scaley = float(y) / TileSizey;

	OneBox.setScale(scalex, scaley);
	TileSizex = x;
	TileSizey = y;
	setPos();
}

void box::undoMove()
{
    pair<int, int> temp;
    if(undo.size()>0)
    {
        temp=undo[undo.size()-1];
        
        changei=temp.first-row;
        changej=temp.second-col;
        row=temp.first;
        col=temp.second;
        move=true;
       
        undo.pop_back();
    }
    
}

void box::setTime(int numberOfMovements, float time)
{
    this->numOfMovements=numberOfMovements;
    this->time=time;

}

void box::update()
{
    
    if(move)
    {
        if(this->clock.getElapsedTime().asSeconds()>time)
        {
            clock.restart();
            OneBox.move(TileSizex/numOfMovements*(changej), TileSizey/ numOfMovements*(changei) );
            
            count++;
        }
        if(count==3)
        {
            count=0;
            move=false;
            setPos();
        }
        
    }
}